
---

# 联系方式

- 手机：18652928213
- Email：psj16@126.com
- QQ/微信号：565144274

---

# 个人信息

 - 潘绳杰/男/1990
 - 本科/大连民族大学自动化系
 - 工作年限：4年
 - 技术博客：http://www.bookcell.org
 - Github: http://github.com/perry2008084
 - 期望职位：Web前端工程师
 - 期望薪资：税前月薪20k
 - 期望城市：南京

---

# 工作经历

## 爱云信息技术 ( 2016年6月 ~ 至今 )

### 物联网IoT PaaS平台

我在此项目中负责前端开发。

涉及技术栈: html/css/js, angularjs, bootstrap, nodejs, gulp

### 公司官网

负责公司宣传网站的开发

涉及技术栈: html/css/js, jquery, bootstrap

## 天派电子 （ 2014年5月 ~ 2016年5月 ）

### 车载多媒体应用软件

我在此项目中负责DVD，Setup，Radio界面应用开发以及调度模块的维护。
在公司一年后，开始负责单个项目的软件担当。

涉及技术栈: C++, Java, Wince和安卓平台

## 南京富士通南大软件（ 2012年6月 ~ 2014年4月 ）

### 存储设备Web配置界面系统

我在此项目中负责前端界面开发和部分后台功能的维护。

涉及技术栈: html/css/js, jquery, c++/csp, cgi

比较有成就的事：
此项目中，最困难的问题是数据量很大时候，页面加载缓慢容易卡死，采取了使用链表并且每次只获取关键信息，减少单次数据量的减小，最后结果是加载比较顺畅。

---

# 开源项目和作品

## 开源项目

 - [天气预报 Hybrid App](https://github.com/perry2008084/bookcellweather) : 一款实用angular和Ionic开发的天气预报app
 - [书格](https://github.com/perry2008084/AndroidProject/tree/master/BookCell) : 一个帮助爱书的人记录书架上书籍的放置，方便寻找的APP。

## 技术文章

- [CSS中background属性的使用指南](http://bookcell.org/2017/04/09/css-background-guide/)
- [JavaScript国际化方案i18next的入门使用](http://bookcell.org/2017/01/20/i18next-tutorial/)

# 技能清单

以下均为我使用的技能,熟练程度由上往下

- Web开发：HTML/CSS/JavaScript/Nodejs
- 前端库及框架：jQuery/Angularjs/vuejs/Expressjs
- 前端工具：Gulp/Webpack/ESLint
- 版本管理工具：Svn/Git
- 数据库相关：MongoDB/SQLServer/SQLite/MySQL

## 参考技能关键字

- web
- javascript/angularjs
- css
- html
- linux
- nodejs

---

# 致谢
感谢您花时间阅读我的简历，期待能有机会和您共事。
